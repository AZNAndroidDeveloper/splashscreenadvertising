package uz.app.lesson36.mainFragment

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import uz.app.lesson36.IntroAdapter
import uz.app.lesson36.R
import uz.app.lesson36.SecondFragment
import uz.app.lesson36.User
import uz.app.lesson36.databinding.FragmentMainBinding


class MainFragment(var mContext: Context?) : Fragment(R.layout.fragment_main) {
    private lateinit var binding: FragmentMainBinding
    private lateinit var introAdapter: IntroAdapter
    private lateinit var indicatorContainer: LinearLayout
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentMainBinding.bind(view)
        userList()
        setIndicator()
        // default indecator tanlanganda
        setUpCurrentIndicatorActive(0)
        binding.viewPager.adapter = introAdapter



    }

    fun userList() {
        val list: MutableList<User> = arrayListOf()
        list.add(User(R.drawable.pic1, "Internetsiz ", getString(R.string.desc3)))
        list.add(User(R.drawable.pic2, "100+", getString(R.string.desc2)))
        list.add(User(R.drawable.pic3, "Tungi interfays ", getString(R.string.desc3)))
        introAdapter = IntroAdapter(list)
    }

    /**
     * indecator yasash
     * */
    private fun setIndicator() {
        indicatorContainer = binding.indicatorContainer
        val indicators = arrayOfNulls<ImageView>(introAdapter.itemCount)
        val layoutParams: LinearLayout.LayoutParams =
            LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT)
        layoutParams.setMargins(8, 0, 8, 0)
        for (i in indicators.indices) {
            indicators[i] = ImageView(requireContext())
            indicators[i]?.setImageDrawable(
                ContextCompat.getDrawable(
                    requireContext(),
                    R.drawable.indicator_inactive
                )
            )
            indicators[i]?.layoutParams = layoutParams
            indicatorContainer.addView(indicators[i])
        }
        binding.viewPager.registerOnPageChangeCallback(object :ViewPager2.OnPageChangeCallback(){
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
            if (position==introAdapter.itemCount-1){
                binding.btnStart.visibility = View.VISIBLE
                binding.tvSkip.visibility = View.INVISIBLE
            }else{
                binding.btnStart.visibility = View.INVISIBLE
                binding.tvSkip.visibility = View.VISIBLE
            }
                setUpCurrentIndicatorActive(position)

            }

        })
        // bu skrolni ochish qaytarilmasligi uchun
        (binding.viewPager.getChildAt(0) as RecyclerView).overScrollMode = RecyclerView.OVER_SCROLL_NEVER
        binding.btnNext.setOnClickListener {
            if (binding.viewPager.currentItem+1<introAdapter.itemCount){
                binding.viewPager.currentItem++
            }
            else{
                val manager = fragmentManager
                manager!!.beginTransaction()
                    .replace(R.id.constr_layout,SecondFragment()).commit()
                binding.btnStart.visibility = View.INVISIBLE
            }
        }
        binding.btnStart.setOnClickListener {
            val manager = fragmentManager
            manager!!.beginTransaction()
                .replace(R.id.constr_layout,SecondFragment()).commit()
            binding.btnStart.visibility = View.INVISIBLE
        }
        binding.tvSkip.setOnClickListener {
            val manager = fragmentManager
            manager!!.beginTransaction()
                .replace(R.id.constr_layout,SecondFragment()).commit()

        }
    }
   private fun setUpCurrentIndicatorActive(position:Int){
       val childCount  = indicatorContainer.childCount

       for (i in 0 until childCount){
           val indicator = indicatorContainer.getChildAt(i) as ImageView
           if (i==position){
               indicator.setImageDrawable(ContextCompat.getDrawable(requireContext(),R.drawable.indicator_active))

           }
           else{
               indicator.setImageDrawable(ContextCompat.getDrawable(requireContext(),R.drawable.indicator_inactive))
           }

       }

    }

}