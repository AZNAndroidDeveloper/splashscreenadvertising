package uz.app.lesson36

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import uz.app.lesson36.databinding.ActivityMainBinding
import uz.app.lesson36.mainFragment.MainFragment

class MainActivity : AppCompatActivity() {
    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        supportFragmentManager.apply {
            beginTransaction().replace(binding.frameLayout.id,MainFragment(this@MainActivity)).commit()
        }
    }

}