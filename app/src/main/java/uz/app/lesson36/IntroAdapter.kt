package uz.app.lesson36

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import org.w3c.dom.Text

 internal class IntroAdapter(val userList:MutableList<User>) : RecyclerView.Adapter<IntroAdapter.IntroViewHolder>() {
     inner class IntroViewHolder(itemView:View):RecyclerView.ViewHolder(itemView){
    val title =itemView.findViewById<TextView>(R.id.titleIntro)
    val desc = itemView.findViewById<TextView>(R.id.descIntro)
    val image = itemView.findViewById<ImageView>(R.id.imageIntro)
    fun bind(user:User){
        title.text = user.title
        desc.text = user.desc
        image.setImageResource(user.image)

    }
}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IntroViewHolder {
        return IntroViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.container_layout,parent,false))

    }

    override fun getItemCount(): Int = userList.size

    override fun onBindViewHolder(holder: IntroViewHolder, position: Int) {

        holder.bind(userList[position])
    }


}
//    private val userList:MutableList<User> = mutableListOf()
//    override fun onCreateViewHolder(
//        parent: ViewGroup,
//        viewType: Int
//    ): IntroViewHolder {
//
//        return IntroViewHolder(ContainerViewBinding.inflate(LayoutInflater.from(parent.context)))
//    }
//
//    override fun getItemCount(): Int = userList.size
//
//    override fun onBindViewHolder(holder: IntroAdapter.IntroViewHolder, position: Int) {
//        val item = userList[position]
//        holder.titleTextView.text = item.title
//        holder.descTextView.text = item.desc
//        holder.image.setImageResource(item.image)
//
//    }
//     class IntroViewHolder(binding:ContainerViewBinding):RecyclerView.ViewHolder(binding.root){
//       val titleTextView:TextView = binding.titleIntro
//        val descTextView:TextView = binding.descIntro
//        val image:ImageView = binding.imageIntro
//    }
//
//    fun setUser(list:MutableList<User>){
//        userList.apply { clear(); addAll(list)
//        notifyDataSetChanged()}
//    }

